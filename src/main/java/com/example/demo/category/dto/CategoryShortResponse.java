package com.example.demo.category.dto;

import com.example.demo.category.CategoryEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CategoryShortResponse {
    @NotNull
    private Long id;
    @NotNull
    private String title;

    public static CategoryShortResponse fromEntity(CategoryEntity categoryEntity) {
        return CategoryShortResponse
                .builder()
                .id(categoryEntity.getId())
                .title(categoryEntity.getTitle())
                .build();
    }
}