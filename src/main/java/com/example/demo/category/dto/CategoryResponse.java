package com.example.demo.category.dto;

import com.example.demo.category.CategoryEntity;
import com.example.demo.todo.dto.ToDoShortResponse;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.stream.Collectors;

@Data
@Builder
@AllArgsConstructor
public class CategoryResponse {
    @NotNull
    private Long id;
    @NotNull
    private String title;

    private List<ToDoShortResponse> toDos;

    public static CategoryResponse fromEntity(CategoryEntity categoryEntity) {
        var toDosDto = categoryEntity
                .getToDos()
                .stream()
                .map(ToDoShortResponse::fromEntity)
                .collect(Collectors.toList());
        return CategoryResponse
                .builder()
                .id(categoryEntity.getId())
                .title(categoryEntity.getTitle())
                .toDos(toDosDto)
                .build();
    }
}