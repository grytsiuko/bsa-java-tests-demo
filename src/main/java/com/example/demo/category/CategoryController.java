package com.example.demo.category;

import com.example.demo.category.dto.*;
import com.example.demo.exception.CategoryNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class CategoryController {

    private final CategoryService categoryService;

    @Autowired
    public CategoryController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @ExceptionHandler({CategoryNotFoundException.class})
    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    public String handleException(Exception ex) {
        return ex.getMessage();
    }

    @GetMapping("/categories")
    @Valid List<CategoryResponse> getAll() {
        return categoryService.getAll();
    }

    @GetMapping("/categories/{id}")
    @Valid CategoryResponse getOne(@PathVariable Long id) throws CategoryNotFoundException {
        return categoryService.getOne(id);
    }

    @PostMapping("/categories")
    @Valid CategoryResponse create(@Valid @RequestBody CategoryCreateRequest categoryCreateRequest) {
        return categoryService.create(categoryCreateRequest);
    }

    @PutMapping("/categories")
    @Valid CategoryResponse update(@Valid @RequestBody CategoryUpdateRequest categoryUpdateRequest)
            throws CategoryNotFoundException {
        return categoryService.update(categoryUpdateRequest);
    }

    @DeleteMapping("/categories/{id}")
    void delete(@PathVariable Long id) throws CategoryNotFoundException {
        categoryService.deleteOne(id);
    }

}