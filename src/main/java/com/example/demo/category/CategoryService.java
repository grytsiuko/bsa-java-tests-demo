package com.example.demo.category;

import com.example.demo.category.dto.*;
import com.example.demo.exception.CategoryNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CategoryService {

    private final CategoryRepository categoryRepository;

    @Autowired
    public CategoryService(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    public List<CategoryResponse> getAll() {
        return categoryRepository
                .findAll()
                .stream()
                .map(CategoryResponse::fromEntity)
                .collect(Collectors.toList());
    }

    public CategoryResponse getOne(Long id) throws CategoryNotFoundException {
        return categoryRepository
                .findById(id)
                .map(CategoryResponse::fromEntity)
                .orElseThrow(() -> new CategoryNotFoundException(id));
    }

    public CategoryResponse create(CategoryCreateRequest categoryDto) {
        var todo = CategoryEntity.fromCreateDto(categoryDto);
        var savedTodo = categoryRepository.save(todo);
        return CategoryResponse.fromEntity(savedTodo);
    }

    public CategoryResponse update(CategoryUpdateRequest categoryUpdateRequest) throws CategoryNotFoundException {
        return categoryRepository
                .findById(categoryUpdateRequest.getId())
                .map(todo -> todo.updateTitle(categoryUpdateRequest.getTitle()))
                .map(categoryRepository::save)
                .map(CategoryResponse::fromEntity)
                .orElseThrow(() -> new CategoryNotFoundException(categoryUpdateRequest.getId()));
    }

    public void deleteOne(Long id) throws CategoryNotFoundException {
        try {
            categoryRepository.deleteById(id);
        } catch (EmptyResultDataAccessException e) {
            throw new CategoryNotFoundException(id);
        }
    }

}
