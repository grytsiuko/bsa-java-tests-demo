package com.example.demo.category;

import com.example.demo.category.dto.CategoryCreateRequest;
import com.example.demo.todo.ToDoEntity;
import lombok.Getter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Entity
@Getter
@Table(name = "categories")
public class CategoryEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id;

    @NotNull
    @Column
    private String title;

    @OneToMany(mappedBy = "category", fetch = FetchType.LAZY, orphanRemoval = true)
    private List<ToDoEntity> toDos = new ArrayList<>();

    public CategoryEntity() {
    }

    public CategoryEntity(String title) {
        this.title = title;
    }

    public CategoryEntity(Long id, String title) {
        this.id = id;
        this.title = title;
    }

    public void appendToDo(ToDoEntity toDo) {
        this.toDos.add(toDo);
    }

    public CategoryEntity(CategoryEntity categoryEntity) {
        this.id = categoryEntity.getId();
        this.title = categoryEntity.getTitle();
        this.toDos = categoryEntity.getToDos();
    }

    public CategoryEntity updateTitle(String title) {
        this.title = title;
        return this;
    }

    public static CategoryEntity fromCreateDto(CategoryCreateRequest categoryCreateRequest) {
        return new CategoryEntity(categoryCreateRequest.getTitle());
    }
}