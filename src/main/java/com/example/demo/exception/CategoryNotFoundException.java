package com.example.demo.exception;

public class CategoryNotFoundException extends Exception {

    public CategoryNotFoundException(Long id) {
        super(String.format("Can not find category with id %d", id));
    }
}