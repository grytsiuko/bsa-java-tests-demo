package com.example.demo.todo;

import com.example.demo.exception.CategoryNotFoundException;
import com.example.demo.exception.ToDoNotFoundException;
import com.example.demo.todo.dto.ToDoCreateRequest;
import com.example.demo.todo.dto.ToDoResponse;
import com.example.demo.todo.dto.ToDoUpdateRequest;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;

@RestController
public class ToDoController {

    private final ToDoService toDoService;

    @Autowired
    public ToDoController(ToDoService toDoService) {
        this.toDoService = toDoService;
    }

    @ExceptionHandler({ToDoNotFoundException.class, CategoryNotFoundException.class})
    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    public String handleException(Exception ex) {
        return ex.getMessage();
    }

    @GetMapping("/todos")
    @Valid List<ToDoResponse> getAll() {
        return toDoService.getAll();
    }

    @GetMapping("/todos/{id}")
    @Valid ToDoResponse getOne(@PathVariable Long id) throws ToDoNotFoundException {
        return toDoService.getOne(id);
    }

    @PostMapping("/todos")
    @Valid ToDoResponse create(@Valid @RequestBody ToDoCreateRequest todoCreateRequest)
            throws CategoryNotFoundException {
        return toDoService.create(todoCreateRequest);
    }

    @PutMapping("/todos")
    @Valid ToDoResponse update(@Valid @RequestBody ToDoUpdateRequest toDoUpdateRequest)
            throws ToDoNotFoundException {
        return toDoService.update(toDoUpdateRequest);
    }

    @PutMapping("/todos/{id}/complete")
    @Valid ToDoResponse complete(@PathVariable Long id) throws ToDoNotFoundException {
        return toDoService.complete(id);
    }

    @DeleteMapping("/todos/{id}")
    void delete(@PathVariable Long id) throws ToDoNotFoundException {
        toDoService.deleteOne(id);
    }

}