package com.example.demo.todo.dto;

import com.example.demo.category.dto.CategoryShortResponse;
import com.example.demo.todo.ToDoEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.time.ZonedDateTime;

import javax.validation.constraints.NotNull;

@Data
@Builder
@AllArgsConstructor
public class ToDoResponse {
    @NotNull
    private Long id;
    @NotNull
    private String text;

    private ZonedDateTime completedAt;

    private CategoryShortResponse category;

    public static ToDoResponse fromEntity(ToDoEntity todoEntity) {
        return ToDoResponse
                .builder()
                .id(todoEntity.getId())
                .text(todoEntity.getText())
                .completedAt(todoEntity.getCompletedAt())
                .category(CategoryShortResponse.fromEntity(todoEntity.getCategory()))
                .build();
    }
}