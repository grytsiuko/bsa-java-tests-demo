package com.example.demo.todo.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ToDoUpdateRequest {
    @NotNull
    private Long id;
    @NotNull
    private String text;
}