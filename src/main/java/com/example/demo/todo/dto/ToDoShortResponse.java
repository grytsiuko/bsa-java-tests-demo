package com.example.demo.todo.dto;

import com.example.demo.todo.ToDoEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.time.ZonedDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ToDoShortResponse {
    @NotNull
    private Long id;
    @NotNull
    private String text;

    private ZonedDateTime completedAt;

    public static ToDoShortResponse fromEntity(ToDoEntity todoEntity) {
        return ToDoShortResponse
                .builder()
                .id(todoEntity.getId())
                .text(todoEntity.getText())
                .completedAt(todoEntity.getCompletedAt())
                .build();
    }
}