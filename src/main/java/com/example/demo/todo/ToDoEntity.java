package com.example.demo.todo;

import com.example.demo.category.CategoryEntity;
import com.example.demo.todo.dto.ToDoCreateRequest;
import lombok.Getter;

import java.time.ZoneOffset;
import java.time.ZonedDateTime;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Getter
@Table(name = "todos")
public class ToDoEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id;

    @NotNull
    @Column
    private String text;

    @Column(name = "completed_at")
    private ZonedDateTime completedAt;

    @NotNull
    @ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.LAZY)
    @JoinColumn(name = "category_id")
    private CategoryEntity category;

    public ToDoEntity() {
    }

    public ToDoEntity(String text, CategoryEntity category) {
        this.text = text;
        this.category = category;
    }

    public ToDoEntity(Long id, String text, CategoryEntity category) {
        this.id = id;
        this.text = text;
        this.category = category;
    }

    public ToDoEntity(Long id, String text, ZonedDateTime completedAt, CategoryEntity category) {
        this.id = id;
        this.text = text;
        this.completedAt = completedAt;
        this.category = category;
    }

    public ToDoEntity(ToDoEntity toDoEntity) {
        this.id = toDoEntity.getId();
        this.text = toDoEntity.getText();
        this.completedAt = toDoEntity.getCompletedAt();
        this.category = toDoEntity.getCategory();
    }

    public ToDoEntity completeNow() {
        this.completedAt = ZonedDateTime.now(ZoneOffset.UTC);
        return this;
    }

    public ToDoEntity updateText(String text) {
        this.text = text;
        return this;
    }

    public static ToDoEntity fromCreateDto(ToDoCreateRequest toDoCreateRequest, CategoryEntity category) {
        return new ToDoEntity(toDoCreateRequest.getText(), category);
    }
}