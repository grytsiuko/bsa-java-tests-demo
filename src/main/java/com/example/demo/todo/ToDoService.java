package com.example.demo.todo;

import java.util.List;
import java.util.stream.Collectors;

import com.example.demo.exception.CategoryNotFoundException;
import com.example.demo.exception.ToDoNotFoundException;
import com.example.demo.category.CategoryRepository;
import com.example.demo.todo.dto.ToDoCreateRequest;
import com.example.demo.todo.dto.ToDoResponse;
import com.example.demo.todo.dto.ToDoUpdateRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

@Service
public class ToDoService {

    private final ToDoRepository toDoRepository;

    private final CategoryRepository categoryRepository;

    @Autowired
    public ToDoService(ToDoRepository toDoRepository, CategoryRepository categoryRepository) {
        this.toDoRepository = toDoRepository;
        this.categoryRepository = categoryRepository;
    }

    public List<ToDoResponse> getAll() {
        return toDoRepository
                .findAll()
                .stream()
                .map(ToDoResponse::fromEntity)
                .collect(Collectors.toList());
    }

    public ToDoResponse getOne(Long id) throws ToDoNotFoundException {
        return toDoRepository
                .findById(id)
                .map(ToDoResponse::fromEntity)
                .orElseThrow(() -> new ToDoNotFoundException(id));
    }

    public ToDoResponse create(ToDoCreateRequest todoDto) throws CategoryNotFoundException {
        var category = categoryRepository
                .findById(todoDto.getCategoryId())
                .orElseThrow(() -> new CategoryNotFoundException(todoDto.getCategoryId()));
        var todo = ToDoEntity.fromCreateDto(todoDto, category);
        var savedTodo = toDoRepository.save(todo);
        return ToDoResponse.fromEntity(savedTodo);
    }

    public ToDoResponse update(ToDoUpdateRequest toDoUpdateRequest) throws ToDoNotFoundException {
        return toDoRepository
                .findById(toDoUpdateRequest.getId())
                .map(todo -> todo.updateText(toDoUpdateRequest.getText()))
                .map(toDoRepository::save)
                .map(ToDoResponse::fromEntity)
                .orElseThrow(() -> new ToDoNotFoundException(toDoUpdateRequest.getId()));
    }

    public ToDoResponse complete(Long id) throws ToDoNotFoundException {
        return toDoRepository
                .findById(id)
                .map(ToDoEntity::completeNow)
                .map(toDoRepository::save)
                .map(ToDoResponse::fromEntity)
                .orElseThrow(() -> new ToDoNotFoundException(id));
    }

    public void deleteOne(Long id) throws ToDoNotFoundException {
        try {
            toDoRepository.deleteById(id);
        } catch (EmptyResultDataAccessException e) {
            throw new ToDoNotFoundException(id);
        }
    }

}
