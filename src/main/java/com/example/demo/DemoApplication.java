package com.example.demo;

import com.example.demo.category.CategoryEntity;
import com.example.demo.todo.ToDoEntity;
import com.example.demo.category.CategoryRepository;
import com.example.demo.todo.ToDoRepository;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;

@SpringBootApplication
public class DemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }

    @Bean
    @Profile("demo")
    CommandLineRunner initDatabase(ToDoRepository toDoRepository, CategoryRepository categoryRepository) {
        return args -> {
            var homeWork = categoryRepository.save(new CategoryEntity("Home Work"));
            var houseWork = categoryRepository.save(new CategoryEntity("House Work"));
            toDoRepository.save(new ToDoEntity("Wash the dishes", houseWork));
            toDoRepository.save(
                    new ToDoEntity("Learn to test Java app", homeWork).completeNow()
            );
        };
    }
}
