insert into categories
    (id, title)
values (1, 'BSA');

insert into todos
    (id, text, category_id, completed_at)
values (1, 'BSA-Spring-Data-HW', 1, {ts '2020-07-09 20:45:23.54'}),
       (2, 'BSA-Testing-HW', 1, null);