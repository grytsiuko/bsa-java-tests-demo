package com.example.demo.category;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles(profiles = "test")
class CategoryIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    void whenGetAll_thenReturnValidResponse() throws Exception {
        this.mockMvc
                .perform(get("/categories"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$[0].id").value(1))
                .andExpect(jsonPath("$[0].title").value("BSA"))
                .andExpect(jsonPath("$[0].toDos").isArray())
                .andExpect(jsonPath("$[0].toDos", hasSize(2)))
                .andExpect(jsonPath("$[0].toDos[0].id").value(1))
                .andExpect(jsonPath("$[0].toDos[0].text").value("BSA-Spring-Data-HW"))
                .andExpect(jsonPath("$[0].toDos[0].completedAt").exists())
                .andExpect(jsonPath("$[0].toDos[1].id").value(2))
                .andExpect(jsonPath("$[0].toDos[1].text").value("BSA-Testing-HW"))
                .andExpect(jsonPath("$[0].toDos[1].completedAt").doesNotExist());
    }

    @Test
    void whenGetOne_thenReturnValidResponse() throws Exception {
        this.mockMvc
                .perform(get("/categories/1"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$").isMap())
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.title").value("BSA"))
                .andExpect(jsonPath("$.toDos").isArray())
                .andExpect(jsonPath("$.toDos", hasSize(2)))
                .andExpect(jsonPath("$.toDos[0].id").value(1))
                .andExpect(jsonPath("$.toDos[0].text").value("BSA-Spring-Data-HW"))
                .andExpect(jsonPath("$.toDos[0].completedAt").exists())
                .andExpect(jsonPath("$.toDos[1].id").value(2))
                .andExpect(jsonPath("$.toDos[1].text").value("BSA-Testing-HW"))
                .andExpect(jsonPath("$.toDos[1].completedAt").doesNotExist());
    }

    @Test
    void whenGetOneNonExistent_thenReturnErrorMessage() throws Exception {
        this.mockMvc
                .perform(get("/categories/1111"))
                .andExpect(status().isNotFound())
                .andExpect(content().contentType("text/plain;charset=UTF-8"))
                .andExpect(content().string("Can not find category with id 1111"));
    }

    @Test
    @Sql(scripts = {"/clean.sql", "/data.sql"}, executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
    void whenCreate_thenReturnValidResponse() throws Exception {
        this.mockMvc
                .perform(post("/categories")
                        .content("{\"title\":\"University\"}")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$").isMap())
                .andExpect(jsonPath("$.id").isNumber())
                .andExpect(jsonPath("$.title").value("University"))
                .andExpect(jsonPath("$.toDos").isArray())
                .andExpect(jsonPath("$.toDos", hasSize(0)));
    }

    @Test
    @Sql(scripts = {"/clean.sql", "/data.sql"}, executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
    void whenUpdate_thenReturnValidResponse() throws Exception {
        this.mockMvc
                .perform(put("/categories")
                        .content("{\"id\": 1, \"title\":\"BSA-2020\"}")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$").isMap())
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.title").value("BSA-2020"))
                .andExpect(jsonPath("$.toDos").isArray())
                .andExpect(jsonPath("$.toDos", hasSize(2)))
                .andExpect(jsonPath("$.toDos[0].id").value(1))
                .andExpect(jsonPath("$.toDos[0].text").value("BSA-Spring-Data-HW"))
                .andExpect(jsonPath("$.toDos[0].completedAt").exists())
                .andExpect(jsonPath("$.toDos[1].id").value(2))
                .andExpect(jsonPath("$.toDos[1].text").value("BSA-Testing-HW"))
                .andExpect(jsonPath("$.toDos[1].completedAt").doesNotExist());
    }

    @Test
    void whenUpdateNonExistent_thenReturnErrorMessage() throws Exception {
        this.mockMvc
                .perform(put("/categories")
                        .content("{\"id\": 1111, \"title\":\"BSA-2020\"}")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound())
                .andExpect(content().contentType("text/plain;charset=UTF-8"))
                .andExpect(content().string("Can not find category with id 1111"));
    }

    @Test
    @Sql(scripts = {"/clean.sql", "/data.sql"}, executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
    void whenDeleteOne_thenReturnValidResponse() throws Exception {
        this.mockMvc
                .perform(delete("/categories/1"))
                .andExpect(status().isOk())
                .andExpect(content().string(""));
        this.mockMvc
                .perform(get("/categories/1"))
                .andExpect(status().isNotFound());
    }

    @Test
    void whenDeleteNonExistent_thenReturnErrorMessage() throws Exception {
        this.mockMvc
                .perform(delete("/categories/1111"))
                .andExpect(status().isNotFound())
                .andExpect(content().contentType("text/plain;charset=UTF-8"))
                .andExpect(content().string("Can not find category with id 1111"));
    }

}
