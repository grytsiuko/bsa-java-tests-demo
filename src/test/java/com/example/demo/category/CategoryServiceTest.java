package com.example.demo.category;

import com.example.demo.category.dto.CategoryCreateRequest;
import com.example.demo.category.dto.CategoryResponse;
import com.example.demo.category.dto.CategoryUpdateRequest;
import com.example.demo.exception.CategoryNotFoundException;
import com.example.demo.todo.ToDoEntity;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.springframework.dao.EmptyResultDataAccessException;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class CategoryServiceTest {

    private CategoryRepository categoryRepository;

    private CategoryService categoryService;

    @BeforeEach
    void setUp() {
        this.categoryRepository = mock(CategoryRepository.class);
        this.categoryService = new CategoryService(categoryRepository);
    }

    @Test
    void whenGetAll_thenReturnAll() {
        //mock
        var category1 = new CategoryEntity(1L, "Category 1");
        var category2 = new CategoryEntity(2L, "Category 2");
        category2.appendToDo(new ToDoEntity(5L, "Item", category2));
        when(categoryRepository.findAll()).thenReturn(List.of(
                new CategoryEntity(category1),
                new CategoryEntity(category2)
        ));

        //call
        var actualList = categoryService.getAll();

        //validate
        var expectedList = List.of(
                CategoryResponse.fromEntity(category1),
                CategoryResponse.fromEntity(category2)
        );
        assertArrayEquals(actualList.toArray(), expectedList.toArray());
    }

    @Test
    void whenGetOne_thenReturnCorrectOne() throws CategoryNotFoundException {
        //mock
        var category = new CategoryEntity(5L, "Category");
        when(categoryRepository.findById(anyLong())).thenAnswer(i -> {
            var id = i.getArgument(0, Long.class);
            return id.equals(category.getId())
                    ? Optional.of(new CategoryEntity(category))
                    : Optional.empty();
        });

        //call
        var actual = categoryService.getOne(category.getId());

        //validate
        var expected = CategoryResponse.fromEntity(category);
        assertEquals(actual, expected);
    }

    @Test
    void whenGetOneNonExistent_thenThrowNotFoundException() {
        var id = 1L;
        assertThrows(
                CategoryNotFoundException.class,
                () -> categoryService.getOne(id)
        );
    }

    @Test
    void whenCreate_thenReturnNew() {
        //mock
        Long createdId = 1L;
        when(categoryRepository.save(ArgumentMatchers.any(CategoryEntity.class))).thenAnswer(i -> {
            var entity = i.getArgument(0, CategoryEntity.class);
            return entity.getId() == null
                    ? new CategoryEntity(createdId, entity.getTitle())
                    : new CategoryEntity();
        });

        //call
        var newTitle = "Created Category";
        var request = new CategoryCreateRequest(newTitle);
        var actual = categoryService.create(request);

        //validate
        var expected = CategoryResponse.fromEntity(new CategoryEntity(createdId, newTitle));
        assertEquals(actual, expected);
    }

    @Test
    void whenUpdate_thenReturnUpdated() throws CategoryNotFoundException {
        //mock
        var category = new CategoryEntity(5L, "Category");
        when(categoryRepository.findById(anyLong())).thenAnswer(i -> {
            var id = i.getArgument(0, Long.class);
            return id.equals(category.getId())
                    ? Optional.of(new CategoryEntity(category))
                    : Optional.empty();
        });
        when(categoryRepository.save(ArgumentMatchers.any(CategoryEntity.class))).thenAnswer(i -> {
            var entity = i.getArgument(0, CategoryEntity.class);
            return entity.getId() != null
                    ? new CategoryEntity(entity)
                    : new CategoryEntity();
        });

        //call
        var updatedTitle = "Updated Category";
        var request = new CategoryUpdateRequest(category.getId(), updatedTitle);
        var actual = categoryService.update(request);

        //validate
        category.updateTitle(updatedTitle);
        var expected = CategoryResponse.fromEntity(category);
        assertEquals(actual, expected);
    }

    @Test
    void whenUpdateNonExistent_thenThrowNotFoundException() {
        var request = new CategoryUpdateRequest(999L, "Updated Category");
        assertThrows(
                CategoryNotFoundException.class,
                () -> categoryService.update(request)
        );
    }

    @Test
    void whenDeleteOne_thenRepositoryDeleteCalled() throws CategoryNotFoundException {
        //call
        var id = 1L;
        categoryService.deleteOne(id);

        //validate
        verify(categoryRepository, times(1)).deleteById(id);
    }

    @Test
    void whenDeleteNonExistent_thenThrowNotFoundException() {
        // mock
        var id = 1L;
        doThrow(new EmptyResultDataAccessException(1)).when(categoryRepository).deleteById(anyLong());

        // call & validate
        assertThrows(
                CategoryNotFoundException.class,
                () -> categoryService.deleteOne(id)
        );
    }

}
