package com.example.demo.todo;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles(profiles = "test")
class ToDoIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    void whenGetAll_thenReturnValidResponse() throws Exception {
        this.mockMvc
                .perform(get("/todos"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].id").value(1))
                .andExpect(jsonPath("$[0].text").value("BSA-Spring-Data-HW"))
                .andExpect(jsonPath("$[0].completedAt").exists())
                .andExpect(jsonPath("$[0].category.id").value(1))
                .andExpect(jsonPath("$[0].category.title").value("BSA"))
                .andExpect(jsonPath("$[1].id").value(2))
                .andExpect(jsonPath("$[1].text").value("BSA-Testing-HW"))
                .andExpect(jsonPath("$[1].completedAt").doesNotExist())
                .andExpect(jsonPath("$[1].category.id").value(1))
                .andExpect(jsonPath("$[1].category.title").value("BSA"));
    }

    @Test
    void whenGetOne_thenReturnValidResponse() throws Exception {
        this.mockMvc
                .perform(get("/todos/1"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$").isMap())
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.text").value("BSA-Spring-Data-HW"))
                .andExpect(jsonPath("$.completedAt").exists())
                .andExpect(jsonPath("$.category.id").value(1))
                .andExpect(jsonPath("$.category.title").value("BSA"));
    }

    @Test
    void whenGetOneNonExistent_thenReturnErrorMessage() throws Exception {
        this.mockMvc
                .perform(get("/todos/1111"))
                .andExpect(status().isNotFound())
                .andExpect(content().contentType("text/plain;charset=UTF-8"))
                .andExpect(content().string("Can not find todo with id 1111"));
    }

    @Test
    @Sql(scripts = {"/clean.sql", "/data.sql"}, executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
    void whenCreate_thenReturnValidResponse() throws Exception {
        this.mockMvc
                .perform(post("/todos")
                        .content("{\"text\":\"Learn some Kotlin\",\"categoryId\": 1}")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$").isMap())
                .andExpect(jsonPath("$.id").isNumber())
                .andExpect(jsonPath("$.text").value("Learn some Kotlin"))
                .andExpect(jsonPath("$.completedAt").doesNotExist())
                .andExpect(jsonPath("$.category.id").value(1))
                .andExpect(jsonPath("$.category.title").value("BSA"));
    }

    @Test
    void whenCreateWithNonExistentCategory_thenReturnErrorMessage() throws Exception {
        this.mockMvc
                .perform(post("/todos")
                        .content("{\"text\":\"Learn some Kotlin\",\"categoryId\": 1111}")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound())
                .andExpect(content().contentType("text/plain;charset=UTF-8"))
                .andExpect(content().string("Can not find category with id 1111"));
    }

    @Test
    @Sql(scripts = {"/clean.sql", "/data.sql"}, executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
    void whenUpdate_thenReturnValidResponse() throws Exception {
        this.mockMvc
                .perform(put("/todos")
                        .content("{\"id\": 1, \"text\":\"BSA-Mini-Project\"}")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$").isMap())
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.text").value("BSA-Mini-Project"))
                .andExpect(jsonPath("$.completedAt").exists())
                .andExpect(jsonPath("$.category.id").value("1"))
                .andExpect(jsonPath("$.category.title").value("BSA"));
    }

    @Test
    void whenUpdateNonExistent_thenReturnErrorMessage() throws Exception {
        this.mockMvc
                .perform(put("/todos")
                        .content("{\"id\": 1111, \"text\":\"BSA-Mini-Project\"}")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound())
                .andExpect(content().contentType("text/plain;charset=UTF-8"))
                .andExpect(content().string("Can not find todo with id 1111"));
    }

    @Test
    @Sql(scripts = {"/clean.sql", "/data.sql"}, executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
    void whenCompleteOne_thenReturnValidResponse() throws Exception {
        this.mockMvc
                .perform(put("/todos/2/complete"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$").isMap())
                .andExpect(jsonPath("$.id").value(2))
                .andExpect(jsonPath("$.text").value("BSA-Testing-HW"))
                .andExpect(jsonPath("$.completedAt").exists())
                .andExpect(jsonPath("$.category.id").value(1))
                .andExpect(jsonPath("$.category.title").value("BSA"));
    }

    @Test
    void whenCompleteNonExistent_thenReturnErrorMessage() throws Exception {
        this.mockMvc
                .perform(put("/todos/1111/complete"))
                .andExpect(status().isNotFound())
                .andExpect(content().contentType("text/plain;charset=UTF-8"))
                .andExpect(content().string("Can not find todo with id 1111"));
    }

    @Test
    @Sql(scripts = {"/clean.sql", "/data.sql"}, executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
    void whenDeleteOne_thenReturnValidResponse() throws Exception {
        this.mockMvc
                .perform(delete("/todos/1"))
                .andExpect(status().isOk())
                .andExpect(content().string(""));
        this.mockMvc
                .perform(get("/todos/1"))
                .andExpect(status().isNotFound());
    }

    @Test
    void whenDeleteNonExistent_thenReturnErrorMessage() throws Exception {
        this.mockMvc
                .perform(delete("/todos/1111"))
                .andExpect(status().isNotFound())
                .andExpect(content().contentType("text/plain;charset=UTF-8"))
                .andExpect(content().string("Can not find todo with id 1111"));
    }

}
