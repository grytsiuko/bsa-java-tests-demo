package com.example.demo.todo;

import static org.junit.jupiter.api.Assertions.*;

import static org.mockito.Mockito.*;

import com.example.demo.exception.CategoryNotFoundException;
import com.example.demo.exception.ToDoNotFoundException;
import com.example.demo.category.CategoryEntity;
import com.example.demo.category.CategoryRepository;
import com.example.demo.todo.dto.ToDoCreateRequest;
import com.example.demo.todo.dto.ToDoResponse;
import com.example.demo.todo.dto.ToDoUpdateRequest;
import org.mockito.ArgumentMatchers;

import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import org.springframework.dao.EmptyResultDataAccessException;

class ToDoServiceTest {

    private ToDoRepository toDoRepository;

    private CategoryRepository categoryRepository;

    private ToDoService toDoService;

    @BeforeEach
    void setUp() {
        this.toDoRepository = mock(ToDoRepository.class);
        this.categoryRepository = mock(CategoryRepository.class);
        this.toDoService = new ToDoService(toDoRepository, categoryRepository);
    }

    @Test
    void whenGetAll_thenReturnAll() {
        //mock
        // We cannot just pass the list to mock, because if service modifies its elements,
        // we will not be able to check it - they will be modified here too
        // So we pass the list of copies to mock
        var category = new CategoryEntity(5L, "Category");
        var toDo1 = new ToDoEntity(1L, "Test1", category);
        var toDo2 = new ToDoEntity(2L, "Test2", ZonedDateTime.now(ZoneOffset.UTC), category);
        when(toDoRepository.findAll()).thenReturn(List.of(
                new ToDoEntity(toDo1),
                new ToDoEntity(toDo2)
        ));

        //call
        var actualList = toDoService.getAll();

        //validate
        var expectedList = List.of(
                ToDoResponse.fromEntity(toDo1),
                ToDoResponse.fromEntity(toDo2)
        );
        assertArrayEquals(actualList.toArray(), expectedList.toArray());
    }

    @Test
    void whenGetOne_thenReturnCorrectOne() throws ToDoNotFoundException {
        //mock
        // the same here and further on - we should return the copy of object from mock
        var category = new CategoryEntity(5L, "Category");
        var toDo = new ToDoEntity(1L, "Test", category);
        when(toDoRepository.findById(anyLong())).thenAnswer(i -> {
            var id = i.getArgument(0, Long.class);
            return id.equals(toDo.getId())
                    ? Optional.of(new ToDoEntity(toDo))
                    : Optional.empty();
        });

        //call
        var actual = toDoService.getOne(toDo.getId());

        //validate
        var expected = ToDoResponse.fromEntity(toDo);
        assertEquals(actual, expected);
    }

    @Test
    void whenGetOneNonExistent_thenThrowNotFoundException() {
        var id = 1L;
        assertThrows(
                ToDoNotFoundException.class,
                () -> toDoService.getOne(id)
        );
    }

    @Test
    void whenCreate_thenReturnNew() throws CategoryNotFoundException {
        //mock
        var category = new CategoryEntity(5L, "Category");
        Long createdId = 1L;
        when(categoryRepository.findById(anyLong())).thenAnswer(i -> {
            var id = i.getArgument(0, Long.class);
            return id.equals(category.getId())
                    ? Optional.of(new CategoryEntity(category))
                    : Optional.empty();
        });
        when(toDoRepository.save(ArgumentMatchers.any(ToDoEntity.class))).thenAnswer(i -> {
            var entity = i.getArgument(0, ToDoEntity.class);
            return entity.getId() == null
                    ? new ToDoEntity(createdId, entity.getText(), entity.getCompletedAt(), entity.getCategory())
                    : new ToDoEntity();
        });

        //call
        var newText = "Created Item";
        var request = new ToDoCreateRequest(newText, category.getId());
        var actual = toDoService.create(request);

        //validate
        var expected = ToDoResponse.fromEntity(new ToDoEntity(createdId, newText, category));
        assertEquals(actual, expected);
    }

    @Test
    void whenCreateWithNotExistentCategory_thenThrowException() {
        var request = new ToDoCreateRequest("Text", 999L);
        assertThrows(
                CategoryNotFoundException.class,
                () -> toDoService.create(request)
        );
    }

    @Test
    void whenUpdate_thenReturnUpdated() throws ToDoNotFoundException {
        //mock
        var category = new CategoryEntity(5L, "Category");
        var toDo = new ToDoEntity(1L, "Old Item", category);
        when(toDoRepository.findById(anyLong())).thenAnswer(i -> {
            var id = i.getArgument(0, Long.class);
            return id.equals(toDo.getId())
                    ? Optional.of(new ToDoEntity(toDo))
                    : Optional.empty();
        });
        when(toDoRepository.save(ArgumentMatchers.any(ToDoEntity.class))).thenAnswer(i -> {
            var entity = i.getArgument(0, ToDoEntity.class);
            return entity.getId() != null
                    ? new ToDoEntity(entity)
                    : new ToDoEntity();
        });

        //call
        var updatedText = "Updated Item";
        var request = new ToDoUpdateRequest(toDo.getId(), updatedText);
        var actual = toDoService.update(request);

        //validate
        toDo.updateText(updatedText);
        var expected = ToDoResponse.fromEntity(toDo);
        assertEquals(actual, expected);
    }

    @Test
    void whenUpdateNonExistent_thenThrowNotFoundException() {
        var request = new ToDoUpdateRequest(999L, "Updated Item");
        assertThrows(
                ToDoNotFoundException.class,
                () -> toDoService.update(request)
        );
    }

    @Test
    void whenComplete_thenReturnWithCompletedAt() throws ToDoNotFoundException {
        //mock
        var category = new CategoryEntity(5L, "Category");
        var toDo = new ToDoEntity(1L, "Test", category);
        when(toDoRepository.findById(anyLong())).thenAnswer(i -> {
            var id = i.getArgument(0, Long.class);
            return id.equals(toDo.getId())
                    ? Optional.of(new ToDoEntity(toDo))
                    : Optional.empty();
        });
        when(toDoRepository.save(ArgumentMatchers.any(ToDoEntity.class))).thenAnswer(i -> {
            var entity = i.getArgument(0, ToDoEntity.class);
            return entity.getId() != null
                    ? new ToDoEntity(entity)
                    : new ToDoEntity();
        });

        //call
        var now = ZonedDateTime.now(ZoneOffset.UTC);
        var result = toDoService.complete(toDo.getId());

        //validate
        assertEquals(result.getId(), toDo.getId());
        assertEquals(result.getText(), toDo.getText());
        assertTrue(result.getCompletedAt().isAfter(now));
    }

    @Test
    void whenCompleteNonExistent_thenThrowNotFoundException() {
        var id = 999L;
        assertThrows(
                ToDoNotFoundException.class,
                () -> toDoService.complete(id)
        );
    }

    @Test
    void whenDeleteOne_thenRepositoryDeleteCalled() throws ToDoNotFoundException {
        //call
        var id = 1L;
        toDoService.deleteOne(id);

        //validate
        verify(toDoRepository, times(1)).deleteById(id);
    }

    @Test
    void whenDeleteNonExistent_thenThrowNotFoundException() {
        // mock
        var id = 1L;
        doThrow(new EmptyResultDataAccessException(1)).when(toDoRepository).deleteById(anyLong());

        // call & validate
        assertThrows(
                ToDoNotFoundException.class,
                () -> toDoService.deleteOne(id)
        );
    }

}
